from flask import Flask, request
from threading import Event
import json

app = Flask(
    __name__,
    static_folder='../static/',
    static_url_path='/static',
)


messages = []
events = []


def wait_for_msg(num):
    if len(messages) > num:
        return
    evt = Event()
    events.append(evt)
    evt.wait()


def add_msg(user, message):
    messages.append({
        'user': user,
        'message': message,
    })
    for evt in events:
        evt.set()


@app.route('/')
def index():
    return app.send_static_file('main.html')


@app.route('/api/send_message/', methods=['POST'])
def send_message():
    user = request.remote_addr
    message = request.form['message']
    add_msg(user, message)
    return ''


@app.route('/api/list_messages/<int:num>/')
def list_messages(num):
    wait_for_msg(num)
    return json.dumps(messages[num:])


app.run(host='0.0.0.0', port=4444, threaded=True)
