(function () {
    var messages = [];

    function displayMsgs() {
        var $msgs = $('.messages ul');
        $msgs.text('');
        messages.forEach(function (msg) {
            var $msg = $('<li></li>');
            $msg.text(msg.user + ': ' + msg.message);
            $msgs.append($msg);
        });
    }

    function getMsgs() {
        $.ajax({
            url: '/api/list_messages/' + messages.length + '/',
            type: 'GET'
        }).success(function (data) {
            messages.push.apply(messages, JSON.parse(data));   
            displayMsgs();
            getMsgs();
        });
    }

    function sendMsg(text) {
        $.ajax({
            url: '/api/send_message/',
            type: 'POST',
            data: {message: text}
        });
    }

    $(document).ready(function () {
        getMsgs();

        $('.send input').keyup(function (e) {
            if (e.keyCode == 13) {
                sendMsg($(this).val());
            }
        });
    });
            
})();
